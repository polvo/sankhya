<?php

require_once(__DIR__ . '/examples/sync/init.php');

function cobrado(PDO $db, int $cod_pedido) {

    $sql = '
        SELECT valor_cobrado
        FROM pedido
        WHERE codigo = ?
    ';

    $query = $db->prepare($sql);
    $query->execute([ $cod_pedido ]);
    $row = $query->fetch(PDO::FETCH_OBJ);

    return (float) $row->valor_cobrado;
}

function getPedidos() {
    $xml = simplexml_load_file(__DIR__ . '/pedidos.xml');
    $out = [];

    $total_pedidos = 0;
    $total_splits = 0;

    foreach ($xml->pedidos->pedido as $pedido_xml) {
        $pedido_id = xml_attr($pedido_xml, 'id');

        if ( array_key_exists($pedido_id, $out) ) {
            $pedido = $out[$pedido_id];
        } else {
            $out[$pedido_id] = $pedido = new stdClass();
            $pedido->total = 0;
            $pedido->desconto = 0;
            $pedido->somaItens = 0;
            $pedido->somaRodape = 0;
            $pedido->somaPagarme = 0;
            $pedido->viaPagarme = false;

            $total_pedidos++;
        }

        $total_splits++;

        $pedido->total += (float) xml_attr($pedido_xml->rodape, 'total');
        $pedido->desconto += (float) xml_attr($pedido_xml->rodape, 'desconto');

        foreach ($pedido_xml->itens->item as $item) {
            $pedido->somaItens += (float) xml_attr($item, 'vlrunitario');
        }

        foreach ($pedido_xml->rodape->financeiros->financeiro as $financeiro) {
            $pedido->somaRodape += (float) xml_attr($financeiro, 'valor');
        }

        foreach ($pedido_xml->regraadicional->financeiro as $financeiro) {
            $pedido->somaPagarme += (float) xml_attr($financeiro, 'valor');
            $pedido->viaPagarme = true;
        }
    }

    echo "TOTAL PEDIDOS: {$total_pedidos}\n";
    echo "TOTAL SPLITS: {$total_splits}\n";

    return $out;
}

$pedidos = getPedidos();

//print_r(reset($pedidos));

foreach ($pedidos as $pedido_id => $pedido) {

    $cobrado = cobrado($db, $pedido_id);
    $a = strval($pedido->total) != strval($pedido->somaItens);
    $b = strval($pedido->somaRodape) != strval($cobrado);
    $c = $pedido->viaPagarme && strval($pedido->somaRodape) != strval($pedido->somaPagarme);

    echo "PEDIDO: {$pedido_id}\n";

    if ( $a || $b || $c ) {
//        continue;
    } else {
        echo "\tOK\n";
    }

    if ( $a ) {
        echo "\ttotal = somaItens : FAIL\n";
    }

    if ( $b ) {
        echo "\tcobrado ({$cobrado}) = somaRodape ($pedido->somaRodape): FAIL\n";
    }

    if ( $c ) {
        echo "\tsomaRodape = somaPagarme : FAIL\n";
    }
}