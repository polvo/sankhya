# API para ERP Sankhya-W

### Instalação 

Adicione ao composer.json:
```json
"repositories": [
  {
    "type": "git",
    "url": "https://bitbucket.org/polvo/sankhya.git"
  }
],
"minimum-stability": "dev",
"require": {
  "polvo/sankhya": "dev-master"
}
```

E as credencias OAuth do bitbucket no seu $HOME/.composer/auth.json

Caso seja sua primeira credencial, BitBucket -> OAuth -> Add Consumer, preencha a "callback url" com "https://example.com", marque como "private consumer" e marque "read" nas permissões de "repository".
```json
{
  "bitbucket-oauth": {
    "bitbucket.org": {
      "consumer-key": "******",
      "consumer-secret": "******"
    }
  }
}
```

### Configuração
Adicione as seguintes variáveis de ambiente:

* SANKHYA_API_URL
* SANKHYA_API_USERNAME
* SANKHYA_API_PASSWORD

Você opcionalmente pode alterar o limite de execução das chamadas da api:
* SANKHYA_REQUEST_TIMEOUT

### Laravel

Para funcionar no laravel adicione o Service Provider dentro do seu *config/app.php*:

```php
'providers' => [
    // providers do framework...
    
    Sankhya\SankhyaServiceProvider::class,
    
    // providers de pacotes...
]
```