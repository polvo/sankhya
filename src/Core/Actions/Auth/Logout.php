<?php


namespace Sankhya\Core\Actions\Auth;


use Sankhya\Core\Action;

class Logout extends Action
{
	protected $prefix = 'MobileLoginSP';
	protected $name = 'logout';
	protected $module = 'mge';

	public function buildRequestBody()
	{
		return null;
	}
}