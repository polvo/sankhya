<?php


namespace Sankhya\Core\Actions\Auth;


use Sankhya\Core\Action;

class Login extends Action
{
	protected $prefix = 'MobileLoginSP';
	protected $name = 'login';
	protected $module = 'mge';

	public function buildRequestBody()
	{
		$credentials = [
			xml('NOMUSU', env('SANKHYA_API_USERNAME')),
			xml('INTERNO', env('SANKHYA_API_PASSWORD')),
		];

		return $credentials;
	}
}