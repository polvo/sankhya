<?php

namespace Sankhya\Core;


use Throwable;

class APIException extends \Exception {
	public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
	{
		if ( empty($message) ) {
			$message = 'Erro desconhecido';
		}

		parent::__construct($message, $code, $previous);
	}
}