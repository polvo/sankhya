<?php
namespace Sankhya\Core;

use GuzzleHttp\Client as Guzzle;
use Sankhya\Core\Actions\Auth\Login;
use Sankhya\Core\Actions\Auth\Logout;

class Client
{
	/**
	 * @var string $sessionID
	*/
	private $sessionID;

	/**
	 * @var bool $debug
	 */
	private $debug;

	/**
	 * @var Guzzle $guzzle
	 */
	private $guzzle;

	public function __construct()
	{
		$this->guzzle = new Guzzle();
	}

	/**
	 * @throws \Exception
	 */
	private function doLogin()
	{
		$response = $this->callService(new Login());

		if ( ! $response->responseBody ) {
			return;
		}

		$this->sessionID = (string) $response->responseBody->jsessionid;
	}

	/**
	 * @param Action $action
	 * @return \SimpleXMLElement
	 * @throws APIException
	 * @throws \Exception
	 */
	private function callService(Action $action)
	{
		$body = $this->buildRequestBody($action);

		$options = $this->getOptions();
		$options['body'] = $body;

		$url = rtrim(env('SANKHYA_API_URL'), '/')
			. '/' . $action->getModule()
			. '/service.sbr'
			. '?serviceName=' . $action->getFullName()
			. '&mgeSession=' . $this->sessionID;

		if ( $this->debug ) {
			echo PHP_EOL;
			echo 'URL: ' . $url . PHP_EOL;
			
			echo PHP_EOL;
			echo 'REQUEST: ' . PHP_EOL . $body . PHP_EOL;
		}

		$response = $this->guzzle->request('POST', $url, $options);

		try {
			$response_raw = (string) $response->getBody();
			$response_xml = simplexml_load_string($response_raw);

			if ( $this->debug ) {
				echo 'RESPONSE: ' . PHP_EOL . $response_raw . PHP_EOL;
			}
		} catch (\Exception $e) {
			throw new \Exception("Parse error: " . $e->getMessage());
		}

		$this->handleError($response_xml);

		return $response_xml;
	}

	private function doLogout()
	{
		if ( empty($this->sessionID) ) {
			return;
		}

		try {
			$this->callService(new Logout());
		} catch (\Exception $e) {
			// pode ser ignorado
		}

		$this->sessionID = null;
	}

	private function buildRequestBody(Action $action): string
	{
		$body = xml('serviceRequest', [
			'serviceName' => $action->getFullName()
		], [
			xml('requestBody', null, $action->buildRequestBody())
		]);

		return $body->asXML();
	}

	/**
	 * @param Action $action
	 * @return \SimpleXMLElement
	 * @throws \Exception
	 */
	public function send(Action $action)
	{
		if ( empty($this->sessionID) ) {
			$this->doLogin();
		}

		try {
			$response = $this->callService($action);
		} catch (\Exception $e) {
			throw $e;
		}

		if ( ! $response instanceof \SimpleXMLElement ) {
			throw new APIException("Invalid response");
		}

		return $response->responseBody->children();
	}

	/**
	 * @param \SimpleXMLElement $response
	 * @throws APIException
	 */
	private function handleError(\SimpleXMLElement $response)
	{
		if ( intval($response->attributes()->status) == 1 ) {
			return;
		}

		$message = utf8_encode(base64_decode(trim(strval($response->statusMessage))));

		throw new APIException($message);
	}

	public function setDebugMode(bool $debug): void
	{
		$this->debug = $debug;
	}

	public function __destruct()
	{
		$this->doLogout();
	}

	private function getOptions(): array
	{
		$cookie = is_null($this->sessionID) ?: 'JSESSIONID=' . $this->sessionID;
		$timeout = (float) env('SANKHYA_REQUEST_TIMEOUT', 10);

		return [
			'headers' => [
				'Content-Type' => 'text/xml',
				'User-Agent' => 'SWServiceInvoker',
				'Cookie' => $cookie
			],
			'connect_timeout' => $timeout
		];
	}
}