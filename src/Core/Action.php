<?php

namespace Sankhya\Core;

use Sankhya\Core\XML\SimpleXMLExtended;

abstract class Action
{
	/**
	 * @var string $version
	 */
	protected $version = '1.0';

	/**
	 * @var string $encoding
	 */
	protected $encoding = 'UTF-8';

	/**
	 * @var string $prefix
	 */
	protected $prefix = 'EcommerceSP';

	/**
	 * @var string $name
	 */
	protected $name;

	/**
	 * @var SimpleXMLExtended
	 */
	protected $content;

	/**
	 * @var string $module
	 */
	protected $module = 'tpeecom';

	/**
	 * Action constructor.
	 * @param SimpleXMLExtended|array|null $content
	 */
	public function __construct($content = null)
	{
		$this->content = $content;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getFullName(): string
	{
		return $this->prefix . '.' . $this->name;
	}

	public function getModule(): string
	{
		return $this->module;
	}

	public function buildRequestBody()
	{
		return xml(
			strtolower($this->name),
			[
				'versao' => $this->version,
				'encoding' => $this->encoding
			],
			$this->content
		);
	}
}