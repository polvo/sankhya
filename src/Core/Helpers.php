<?php

use Sankhya\Core\XML\CData;
use Sankhya\Core\XML\SimpleXMLExtended;

function is_vector(array $arr) {
	foreach ($arr as $key => $_) {
		if ( ! is_int($key) ) return false;
	}
	return true;
}

function xml_encode($str) {
	return utf8_encode($str);
}

/**
 * @param string $name
 * @param array | string $data
 * @param mixed $children
 * @return SimpleXMLExtended
 */
function xml(string $name, $data = null, $children = null): SimpleXMLExtended {
	$node = new SimpleXMLExtended('<'.$name.'/>');

	if ( $data instanceof CData ) {
		$node->addCData($data);
	}
	else if ( ( is_string($data) || is_numeric($data) ) && strlen($data) > 0 ) {
		$node->addXMLString(xml_encode(strval($data)));
	}
	else if ( is_array($data) ) {

		if ( is_vector($data) ) {
			$children = $data;
		} else {
			foreach ($data as $key => $value) {
				if ( is_null($value) ) {
					continue;
				}

				$node->addAttribute($key, xml_encode($value));
			}
		}

	}

	if ( $children ) {
		if ( ! is_array($children) ) {
			$children = [ $children ];
		}

		foreach ($children as $child) {
			if ( $child instanceof SimpleXMLElement ) {
				$node->append($child);
			}
			elseif ( $child instanceof CData ) {
				$node->addCData($child);
			}
			elseif ( is_null($child) ) {
				continue;
			}
			else {
				$node->addXMLString(xml_encode($child));
			}
		}
	}

	return $node;
}

function cdata(string $value) {
	return new CData(xml_encode($value));
}

if ( ! function_exists('env') ) {
	function env($key, $default = null) {
		return getenv($key) ?? $default;
	}
}

function xml_attr(SimpleXMLElement $xml, string $attr) {
	return utf8_encode(strval($xml->attributes()->{$attr}));
}

function money(float $amount) {
	return round($amount, 2);
}

function getProporcional(float $valor, float $percentual) {
	return money($valor * $percentual);
}

function cloneObj(\stdClass $obj, bool $deep = true) {
	$clone = new stdClass();

	foreach ($obj as $key => $value) {
		$clone->{$key} = ( $deep && $value instanceof \stdClass)
            ? cloneObj($value)
            : $value;
	}

	return $clone;
}

function iso_to_date($str) {
	if ( is_null($str) ) return null;
	return preg_replace('/^(\d{4})-(\d{2})-(\d{2})(.*)/', '$3/$2/$1', $str);
}