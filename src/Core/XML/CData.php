<?php

namespace Sankhya\Core\XML;

class CData
{
	/**
	 * @var string $value
	 */
	public $value;

	public function __construct(string $value)
	{
		$this->value = $value;
	}
}