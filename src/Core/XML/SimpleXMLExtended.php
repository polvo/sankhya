<?php

namespace Sankhya\Core\XML;

use SimpleXMLElement;

class SimpleXMLExtended extends SimpleXMLElement {
	public function addCData(CData $cdata)
	{
		$this->appendChild(function (\DOMDocument $owner) use ($cdata) {
			return $owner->createCDATASection($cdata->value);
		});
	}

	public function addXMLString(string $text)
	{
		$this->appendChild(function (\DOMDocument $owner) use($text) {
			$fragment = $owner->createDocumentFragment();
			$fragment->appendXML($text);
			return $fragment;
		});
	}

	public function append(SimpleXMLElement $child)
	{
		$this->appendChild(function (\DOMDocument $owner) use($child) {
			$child = dom_import_simplexml($child);
			return $owner->importNode($child, true);
		});
	}

	private function appendChild(callable $fn)
	{
		$node = dom_import_simplexml($this);
		$owner = $node->ownerDocument;
		$node->appendChild($fn($owner));
	}

	public function getParent(): SimpleXMLExtended
	{
		return current($this->xpath('parent::*'));
	}
}